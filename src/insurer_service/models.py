from django.db import models
from django.db.models import Sum

from .managers import ActiveObjectsManager


class Audit(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, db_index=True)

    class Meta:
        abstract = True


class SoftDelete(models.Model):
    is_active = models.BooleanField(default=True, db_index=True)
    objects = models.Manager()
    active_objects = ActiveObjectsManager()

    def delete(self, *args, **kwargs):
        self.is_active = False
        self.save()

    class Meta:
        abstract = True


class Policy(Audit, SoftDelete):
    external_user_id = models.CharField(max_length=100)
    benefit = models.CharField(max_length=100)
    currency = models.CharField(max_length=100)
    total_max_amount = models.IntegerField()

    @property
    def total_allowed_amount(self):
        total_withdrawn = self.payments.filter(status=Payment.accepted_status_choice).aggregate(
            Sum('amount')).get('amount__sum') or 0
        return self.total_max_amount - total_withdrawn

    class Meta:
        unique_together = ['external_user_id', 'benefit', 'currency', 'is_active']


class Payment(Audit):
    declined_status_choice = 0
    accepted_status_choice = 1

    status_choices = (
        ('Declined', declined_status_choice),
        ('Accepted', accepted_status_choice)
    )

    policy = models.ForeignKey(Policy, related_name='payments', on_delete=models.CASCADE)
    amount = models.IntegerField()
    timestamp = models.DateTimeField()
    status = models.SmallIntegerField(choices=status_choices)
