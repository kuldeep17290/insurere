from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from .models import Policy


class PolicySerializer(serializers.ModelSerializer):
    def validate(self, attrs):
        if Policy.objects.filter(external_user_id=attrs['external_user_id'], benefit=attrs['benefit'],
                                 currency=attrs['currency']).exists():
            raise ValidationError('Policy already exists')
        return attrs

    class Meta:
        model = Policy
        fields = ('external_user_id', 'benefit', 'currency', 'total_max_amount')


class PaymentSerializer(serializers.Serializer):
    external_user_id = serializers.CharField(max_length=100)
    benefit = serializers.CharField(max_length=100)
    currency = serializers.CharField(max_length=100)
    amount = serializers.IntegerField()
    timestamp = serializers.DateTimeField()
