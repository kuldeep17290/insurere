from rest_framework import generics, status
from rest_framework.response import Response

from .models import Policy, Payment
from .serializers import PolicySerializer, PaymentSerializer


class PolicyView(generics.CreateAPIView):
    queryset = Policy.active_objects.all()
    serializer_class = PolicySerializer


class PaymentView(generics.CreateAPIView):
    serializer_class = PaymentSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        policy = Policy.active_objects.filter(external_user_id=serializer.data['external_user_id'],
                                              benefit=serializer.data['benefit'],
                                              currency=serializer.data['currency']).last()
        requested_amount = serializer.data['amount']
        timestamp = serializer.data['timestamp']
        if policy:
            if policy.total_allowed_amount < requested_amount:
                Payment.objects.create(policy=policy, amount=requested_amount, status=Payment.declined_status_choice,
                                       timestamp=timestamp)
                data = {"authorized": False, "reason": 'POLICY_AMOUNT_EXCEEDED'}
            else:
                Payment.objects.create(policy=policy, amount=requested_amount, status=Payment.accepted_status_choice,
                                       timestamp=timestamp)
                data = {"authorized": True, "reason": None}
        else:
            data = {"authorized": False, "reason": 'POLICY_NOT_FOUND'}
        return Response(data, status=status.HTTP_200_OK)
